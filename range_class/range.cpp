#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class range{
    private:
        int start;
        int limit;

    public:

    range(){
        start = 0;
        limit = 1;
    }
    
    range(int b){
        start = 0;
        limit = b;
    }

    range(int a, int b){
        start = a;
        limit = b;
    }

    int getStart(){
        return start;
    }

    int getLimit(){
        return limit;
    }

    void shift(int n){
        cout << "IN shift st " << start << " limit " << limit << endl;
        start += n;
        limit += n;
    }

    void reset(){
        start = 0;
        limit= 1; 
    }

    void squeeze(){
        ++start;
        --limit;
        cout << "In squeeze st " << start << "limit " << limit << endl; 

        if (limit < start)
            reset();
    }

    void squeeze(int n){
        start -= n;
        limit -= n;

        if (limit < start)
            reset();
    }
    
    range getCommon(range n){
        if (isDisjoint(n))
            return range();

        vector<int> rngs;
        rngs.push_back(this -> start);
        rngs.push_back(this -> limit);
        rngs.push_back(n.start);
        rngs.push_back(n.limit);
        sort(rngs.begin(), rngs.end());
        return range(rngs[1], rngs[2]); 
    }

    bool contains(range n){
        return (this -> start <= n.start <= this -> limit) && (this -> start <= n.limit <= this -> limit);
    }
    
    bool contains(int n){
        return (this -> start <= n <= this -> limit);
    }

    bool isOverlapping(range n){
        return (this -> start <= n.start <= this -> limit) || (this -> start <= n.limit <= this -> limit);
    }

    bool isDisjoint(range n){
        return !(isOverlapping(n));
    }

    bool isLessThan(range n){
        return this -> start < n.start;
    }

    bool isTouching(range n){
        return this -> limit == n.start;
    }

    virtual int len(){
        return limit - start;
    }

    

};

class closedInterval: public range{
    public:
        closedInterval(): range(0, 0){
            ;
        }

        closedInterval(int limit): range(limit + 1){
            ;
        }
        
        closedInterval(int start, int limit): range(start, limit + 1){
            ;
        }

        closedInterval merge(closedInterval n){
            if (isDisjoint(n))
                return closedInterval();
            return closedInterval(min(getStart(), n.getStart()), max(getLimit(), n.getLimit()) - 1);
        }

        int len(){
            return getLimit() - getStart();
        }

        /*   
        bool isTouching(range n){
            return this -> end + 1 == n.start;
        }
        */
};

int main(){
    //range c(4);
    //range b(5, 7);
    //range e;
    //closed rng(4);
    //range *rng;
    closedInterval eg(4, 10);
    closedInterval ie(10, 20);
    //rng = &eg;
    cout << "len " << eg.len() << endl;
    closedInterval cur = ie.merge(eg);
    cout << "len of merged interval " << cur.len() << endl;
    //4 5 6 7 8 9 10 = 7
    //rng.squeeze();
    //cout << "len fter squzing " << rng.len() + 1 << endl;
    //cout << "touching c & r " << rng.isTouching(b) << endl;
    /*
    vector<int> c_nums = c.getNumsInRange();
    vector<int> b_nums = b.getNumsInRange();
    cout << "touching " << c.isTouching(b) << endl;
    cout << "contains " << b.contains(6) << " " << c.contains(b) << endl;
    //cout << "combine " << c.combine(b) << endl;
    //cout << "intersection " << b.getCommon(c) << endl;
    cout << "overlaps " << b.isOverlapping(c) << endl;
    cout << "len of e" << e.len() << endl;
    range modified = c.merge(e);
    range d = b.getCommon(c);
    cout << "len of modified" << modified.len() << endl;
    modified.squeeze();
    cout << "len of modified" << modified.len() << endl;
    d.shift(5);
    cout << "len of shifed d" << d.len() << endl;
    cout << "is e touching b " << e.isTouching(b) << endl;
    vector<int> m_nums = d.getNumsInRange();
    for (int i = 0; i < m_nums.size(); ++i)
        cout << m_nums[i] << " ";
    cout << endl;
    */
    return 0;

}
